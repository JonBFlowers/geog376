####################################################################### 
#Author: Jonathan FLowers
#Program name: lab08.py 
#Date: 08/14/2016 
#Variables used: path, station, year, months, lat, lon, theURL, infile, indata
#outfile, outFile, inf, data, data2, data3, data4, parts, header, varList
#shortVarList, body, bodyList, outf, m, p, line, newline
#This program takes weather data from a url and prints it out onto a new,
#properly formated data file.
#########################################################################

import urllib
print "Start... "

#Set path, station, and useful variables
path = "C:\Users\Jonathan Flowers\Desktop\Lab08/"
station = "KMDCOLLE6"
year = 2015
months = [1, 4, 9]
lat = 39.179
lon = -76.962

#loop downloads the data from each month
for m in months:
    #creates URL for each month
    theURL = 'https://www.wunderground.com/weatherstation/WXDailyHistory.asp?ID={ws}&year={year}&month={m}&graphspan=month&format=1'.format(ws = station, m = str(m),year=str(year)) 
    print theURL
    infile = urllib.urlopen(theURL)
    indata = infile.read()
    #creates output file for each month
    outfile = open(path + "{ws}_{yyyy}{mm}.txt".format(ws = station, yyyy = str(year), mm = (str(m)).zfill(2)), 'w')
    outfile.write(indata)
    outfile.close()
    infile.close()

print "Finished dowloading data" #indicates when first section is finished


path = "C:\Users\Jonathan Flowers\Desktop\Lab08/"
outFile= "C:\Users\Jonathan Flowers\Desktop\Lab08\weather_data_all.txt"
n = 1

#loops through each month
for month in months:
    #sets input file as a particular month
    infile = path+"{ws}_{year}{mm}.txt".format(ws = station,year = year, mm = (str(month)).zfill(2))
    #obtains and cleans data
    inf = open(infile, 'r')
    data = inf.read()
    data2 = data.replace("<br>","")
    data3 = data2.strip("\n")
    data4 = data3.replace("\n\n","\n")
    #partitions data and sets the header
    parts = data4.partition("\n")
    header = parts[0]
    #creates new header
    if n==1:
        varList = ['Temperature','Dewpoint','Pressure','Humidity','WindSpeed','GustSpeed','Precipitation']
        shortVarList = ['T','DP','P','RH','WS','GS','Pr']
        for i in range(len(varList)):
            newheader = header.replace(varList[i],shortVarList[i])
            header = newheader
        #Links header to outfile
        outf = open(outFile,'w')
        outf.write(newheader+",lat_lon,keeper\n")
        outf.close()
        n=n+1
    #splits obtained data
    body = parts[2]
    bodyList = body.split("\n")
    outf = open(outFile,'a')
    #writes data to output file
    for p in range (len(bodyList)):
        line = bodyList[p]
        if line != "":
            newLine = line+',{lat} : {lon}, {keep}\n'.format(lat = str(lat), lon = str(lon), keep = "Jonathan Flowers")
            outf.write(newLine)
    #close files 
    outf.close()
    inf.close()

#indicates program is completed
print "Finished cleaning the files."