####################################################################### 
#Author: Jonathan FLowers
#Program name: lab06.py 
#Date: 08/2/2016 
#Variables used: extent, west, south, north, east, width, height, x_step
#y_step, polygonlist, p1, p2, p3, p4, poly, feat, out, dis, area, sr_world
#expr, count, result
#This program projects a grid on the top half of a shapefile of Maryland's counties,
#from there the area is calculated and printed to screen
#######################################################################
import arcpy
import math
arcpy.env.workspace = r"C:\Users\Jonathan Flowers\Desktop\Lab06_data\Lab06_data\part2/" #Sets workspace
arcpy.env.overwriteOutput = True #Overwrite any previously created files
#Gets the extent of MD_County_2014
extent = arcpy.Describe("C:\Users\Jonathan Flowers\Desktop\lab06_data\lab06_data\part2\MD_County_2014.shp").extent

#spot Coordinates to help define the grid
west = extent.XMin
south = extent.YMin
east = extent.XMax
north = extent.YMax
width = extent.width
height = extent.height

south = (float(north) + float(south))/2 #makes it so only half of grid is used
#Breaks height and width up so that there are 120 rectangles
width = width/12 
height = height/10


x_step = int((east - west)/12) + 1 #sets up the step for the x and y axis
y_step = int((north- south)/10) + 1

polygonList = [] 
# define x, y values 
for y in range(int(north), int(south), -y_step): #incasing loop that increments y axis after each x axis is done
    for x in range(int(west), int(east), x_step): #loop that increments x axis after grid is made
        cornersArray = arcpy.Array() 
        # define and add the first point 
        p1 = arcpy.Point()
        p1.X = x 
        p1.Y = y 
        cornersArray.add(p1) 
        # define and add the second point 
        p2 = arcpy.Point() 
        p2.X = x + width
        p2.Y = y 
        cornersArray.add(p2) 
        # define and add the third point 
        p3 = arcpy.Point() 
        p3.X = x + width
        p3.Y = y - height
        cornersArray.add(p3) 
        # define and add the fourth point 
        p4 = arcpy.Point() 
        p4.X = x 
        p4.Y = y - height
        cornersArray.add(p4) 
        #creates array and adds it to the array list
        poly = arcpy.Polygon(cornersArray) 
        polygonList.append(poly)
               

feat = '12_deg_grid.shp' #Grid file
out = "C:\Users\Jonathan Flowers\Desktop\lab06_data\lab06_data\part2\MD_County_2014_out.shp" #output shapefile
dis = "C:\Users\Jonathan Flowers\Desktop\lab06_data\lab06_data\part2\MD_County_2014_dis.shp" #dissolve ouput file
area = "C:\Users\Jonathan Flowers\Desktop\lab06_data\lab06_data\part2\MD_County_2014_area.shp" #calculate area output file
arcpy.CopyFeatures_management(polygonList, feat) #Copys array list to the grid file
#describes MD_County_2014 shape file
sr_world = arcpy.Describe("C:\Users\Jonathan Flowers\Desktop\lab06_data\lab06_data\part2\MD_County_2014.shp").spatialReference
#Defines projection for grid file and then counts the number of values in it and prints
arcpy.DefineProjection_management(feat, sr_world) 
count = arcpy.GetCount_management(feat).getOutput(0)
print "There are {} rectangles.".format(count) 

#Adds UID to created grid and calculates the values
arcpy.AddField_management(feat, "UID","LONG", 10, 0) 
arcpy.CalculateField_management(feat, "UID", '!FID!+1', 'PYTHON')

#Intersects MD_County shapefile with grid file and then dissolves them together
arcpy.Intersect_analysis([feat,"C:\Users\Jonathan Flowers\Desktop\lab06_data\lab06_data\part2\MD_County_2014.shp"],out, "ALL")
arcpy.Dissolve_management(out, dis, "UID")

#Adds field Co_AREA to area file
expr = '("Co_AREA" < 2500000000)'
arcpy.CalculateAreas_stats(dis, area) 
arcpy.AddField_management(area, "Co_AREA", "FLOAT", 10,3) 
arcpy.CalculateField_management(area, "Co_AREA", '!F_AREA!', 'PYTHON') 
arcpy.MakeFeatureLayer_management(area, "Co_AREA")

#finds and prints out how many values meets a discription that was set
arcpy.SelectLayerByAttribute_management("Co_AREA", "", expr) #performs SQL statement
result = float(arcpy.GetCount_management("Co_AREA").getOutput(0))
print "There are {} values that are less than 2500000000 in the Co_AREA field.".format(int(result))