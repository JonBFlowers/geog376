This repository contains examples of projects and labs that I have done in my GIS classes at the University of Maryland

Lab06 - This program projects a grid on to the top half of a shape-file of Maryland's counties.

Lab08 - This program takes weather data from a url and prints it out onto a new, properly formatted data file.